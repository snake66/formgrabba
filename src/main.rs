// formgrabba - simple html form processor
// Copyright (C) 2017  Harald Eilertsen <haraldei@anduin.net>
//
// This program is free software: you can redistribute it and/or modify
// it under the terms of the GNU General Public License as published by
// the Free Software Foundation, either version 3 of the License, or
// (at your option) any later version.
//
// This program is distributed in the hope that it will be useful,
// but WITHOUT ANY WARRANTY; without even the implied warranty of
// MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
// GNU General Public License for more details.
//
// You should have received a copy of the GNU General Public License
// along with this program.  If not, see <http://www.gnu.org/licenses/>.

extern crate chrono;
extern crate httparse;
extern crate serde_urlencoded as urlencoded;

use chrono::prelude::*;

use std::collections::HashMap;
use std::fs::{self, File};
use std::io::{Read, Write};
use std::iter::FromIterator;
use std::os::unix::net::{UnixListener, UnixStream};
use std::os::unix::fs::PermissionsExt;
use std::path::Path;
use std::result::Result;
use std::thread;

fn read_file(filename: &str) -> std::io::Result<String> {
    let mut f = File::open(filename)?;
    let mut body = String::new();
    f.read_to_string(&mut body)?;
    Ok(body)
}

fn make_response_headers(version: &u8, body: &String) -> String {
    let now : DateTime<Utc> = Utc::now();
    let mut hdrs = String::new();
    hdrs += &format!("HTTP/1.{} 200 OK\r\n", version);
    hdrs += &format!("Date: {}\r\n", now.format("%a, %d %h %Y %T GMT"));
    hdrs += "Content-Type: text/html; charset=utf-8\r\n";
    hdrs += &format!("Content-Length: {}\r\n", body.len());
    hdrs += "\r\n";
    hdrs
}

fn serve_file(stream: &mut UnixStream, version: &u8, filename: &str)
    -> std::io::Result<()>
{
    let body = read_file(filename)?;
    let hdrs = make_response_headers(&version, &body);
    stream.write_all(hdrs.as_bytes())?;
    stream.write_all(body.as_bytes())?;

    Ok(())
}

fn decode_body(body: &[u8]) -> Result<HashMap<String, String>, urlencoded::de::Error>
{
    urlencoded::from_bytes(body)
}

fn process_req(mut stream: &mut UnixStream, req: httparse::Request, body: &[u8])
{
    // Should be safe to unwrap since we only get here when
    // request is complete
    let path = req.path.unwrap();
    let method = req.method.unwrap();
    let version = req.version.unwrap();

    if path == "/" {
        match method {
            "GET" => {
                serve_file(&mut stream, &version, "form.html")
                    .unwrap_or_else(|e| println!("Failed to serve file: {:?}", e));
            },
            "POST" => {
                // Turn headers array into tuples of (name, value)
                let hdr_iter = req.headers
                    .into_iter()
                    .map(|h| (h.name, String::from_utf8_lossy(h.value)));

                // Turn the tuples into a HashMap
                let hdrs = HashMap::<&str, std::borrow::Cow<str>>::from_iter(hdr_iter);
                let content_length = match hdrs.get("Content-Length") {
                    Some(num) => usize::from_str_radix(num, 10).unwrap(),
                    None => 0
                };

                let content = decode_body(&body[..content_length]).unwrap();
                println!("body:");
                for item in content {
                    println!(" {:?}", item);
                }
            },
            _ => {
                println!("Unsupported method {}, ignoring!", method);
            }
        }
    }
}


fn handle_client(mut stream: UnixStream) {
    let mut data = Vec::<u8>::new();
    let mut buf = [0u8; 256];

    loop {
        match stream.read(&mut buf) {
            Ok(size) => {
                println!("Reading {} bytes...", size);
                data.extend_from_slice(&buf);
                if size < buf.len() {
                    break;
                }
            }
            Err(e) => {
                println!("Error reading stream: {:?}", e);
                return;
            }
        }
    }

    let mut hdrs = [httparse::EMPTY_HEADER; 16];
    let mut req = httparse::Request::new(&mut hdrs);
    match req.parse(&data) {
        Ok(httparse::Status::Complete(size)) => {
            println!("Received request of {} bytes...", size);
            process_req(&mut stream, req, &data[size..]);
        },
        Ok(httparse::Status::Partial) => {
            println!("Incomplete request, ignoring!");
        },
        Err(e) => {
            println!("Invalid request: {}", e);
        }
    }
}

fn main() {
    let sock = Path::new("/tmp/grabba.sock");

    if sock.exists() {
        fs::remove_file(sock).unwrap();
    }

    let listener = UnixListener::bind(&sock).unwrap();

    let mut perms = fs::metadata(sock).unwrap().permissions();
    perms.set_mode(0o766);
    fs::set_permissions(sock, perms).unwrap();

    let addr = listener.local_addr().expect("Local addr failed");
    println!("Listening on socket: {}", addr.as_pathname().unwrap().to_str().unwrap());

    for stream in listener.incoming() {
        match stream {
            Ok(stream) => {
                println!("Connected");
                thread::spawn(move || handle_client(stream));
            },
            Err(e) => {
                println!("Stream error: {:?}", e);
            }
        };
    }

    fs::remove_file(sock).unwrap();
}
